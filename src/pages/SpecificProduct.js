import { useState, useEffect } from 'react';
import { Container, Card } from 'react-bootstrap';

export default function SpecificProduct({match}) {
	// console.log(match)

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	
	// match.params holds the ID of our course in the productId property
	const productId = match.params.productId;
	// console.log(productId);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [])

	return(
		<Container className="mt-5">
			<Card className="mb-2">
	       	    <Card.Body>
	       	        <Card.Title>{name}</Card.Title>
	       	        <Card.Subtitle>Description:</Card.Subtitle>
	       	        <Card.Text>{description}</Card.Text>
	       	        <Card.Subtitle>Price:</Card.Subtitle>
	       	        <Card.Text>PhP {price}</Card.Text>
	       	    </Card.Body>
	       	</Card>
		</Container>


	)
}
